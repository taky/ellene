README
=======

Copyright (C) 2012 Japan Android Group Saitama Branch.  All rights reserved.

This is photo-sharing application.


0. HOW TO BUILD
=================

[Fire emulator/device up...]

$ ant clean debug uninstall install
...
BUILD SUCCESSFUL
Total time: xx seconds


If you have @alterakey's lil' launcher script installed somewhere in your PATH,
then you can use the 'run' Ant task to quickly run/test it.

It is available at: https://gist.github.com/1223663 .


1. FEATURES
=============

 * Images can be loaded from standard apps, Gallery or such
 * Images can quickly be shared with Bluetooth over NFC/QR


2. BUGS
========

 * Image gallery is choppy, and cannot zoom into images.


3. ACKNOWLEDGES
=================

This project is conducted in works of JAG Satama Branch Workshop, namely the Android Mash-up #1.

@alterakey: Code
@inda_re: Code
@jKaro: Icon
@nekosukesan55: Basic ideas, meeting setup etc.
