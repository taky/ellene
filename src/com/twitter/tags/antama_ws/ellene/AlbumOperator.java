package com.twitter.tags.antama_ws.ellene;

public interface AlbumOperator {
    void cancel();
    void rewrite();
    Album get();
    void replace(Album newAlbum);
    void delete();
    void rename(String name);
}
