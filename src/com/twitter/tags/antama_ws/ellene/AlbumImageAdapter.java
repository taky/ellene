package com.twitter.tags.antama_ws.ellene;

import java.io.*;
import java.util.*;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.*;
import android.view.*;
import android.widget.*;

public class AlbumImageAdapter extends BaseAdapter {
    private Context c;
    private Album album;
    private int galleryItemBackgroundResource;

    public AlbumImageAdapter(Context c, Album album) {
        this.c = c;
        this.album = album;
        TypedArray attr = this.c.obtainStyledAttributes(R.styleable.Ellene);
        this.galleryItemBackgroundResource = attr.getResourceId(
            R.styleable.Ellene_android_galleryItemBackground, 0);
        attr.recycle();

    }

    public int getCount() {
        return this.album.list().size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = (ImageView)convertView;
        List<File> files = this.album.list();

        if (imageView == null) {
            imageView = new ImageView(this.c);
        }
        else {
            this.recycle(imageView);
        }

        try {
            File file = files.get(position);
            BitmapDrawable drawable = new ThumbnailCache(this.c, this.album).get(file);

            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setBackgroundResource(this.galleryItemBackgroundResource);
        }
        catch (IOException e) {
            this.recycle(imageView);
        }

        return imageView;
    }

    private void recycle(ImageView view) {
        Drawable drawable = view.getDrawable();
        view.setImageDrawable(new ColorDrawable(android.R.color.transparent));
        if (drawable != null) {
            try {
                ((BitmapDrawable)drawable).getBitmap().recycle();
            }
            catch (ClassCastException e) {
            }
            catch (NullPointerException e) {
            }
            drawable = null;
        }
    }
}
