package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.Toast;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.util.Log;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import android.content.Intent;
import android.bluetooth.*;

import java.io.*;
import java.net.*;

public class PushActivity extends Activity {
    private NFCSender sender;
    private Album album;

    private BluetoothListener listener = new BluetoothListener();

    private static final int ENABLE_BT_REQUEST = 1;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.push);

        new WakeLock(this).hold();

        this.album = new AlbumLoader(this).load();

        if (!this.album.isAvailable()) {
            new StorageAlert(this).show();
            return;
        }

        if (this.album.empty()) {
            Toast.makeText(this, getString(R.string.toast_no_empty_push), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        this.sender = new NFCSender(this, this.album);

        this.sender.init();
        this.sender.send(null);

        this.startPush();
    }

    private void startPush() {
        try {
            BluetoothAdapter adapter = new BluetoothManipulator().setup();
            String address = adapter.getAddress();
            this.sender.send(address);
            Log.d("MA.sB", String.format("Bluetooth address: %s", address));

            try {
                ImageView qr = (ImageView)findViewById(R.id.qr);
                BitmapDrawable drawable = new QRSender(this, this.album).send(address);
                qr.setImageDrawable(drawable);
            }
            catch (com.google.zxing.WriterException e) {
                Log.d("PA", String.format("QR code generation failed: %s", e.getMessage()));
            }
        }
        catch (BluetoothManipulator.NotSupportedException e) {
            Log.d("MA.sB", "bluetooth is not supported");
            finish();
        }
        catch (BluetoothManipulator.NotEnabledException e) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case ENABLE_BT_REQUEST:
            if (resultCode == RESULT_OK) {
                this.startPush();
            }
            else {
                Log.d("MA.sB", "bluetooth cannot be enabled");
                finish();
            }
            break;
        default:
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.sender.resume();

        this.listen();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.sender.pause();

        this.unlisten();
    }

    private void unlisten() {
        this.listener.stop();
    }

    private void listen() {
        try {
            this.listener.start(this.album, new ConnectionListener());
        }
        catch (IOException e) {
            Log.d("MA.listen", String.format("cannot listen connection: %s", e.toString()));
        }
        catch (BluetoothManipulator.NotSupportedException e) {
            Log.d("MA.listen", "bluetooth is not supported on this device");
        }
        catch (BluetoothManipulator.NotEnabledException e) {
            Log.d("MA.listen", "bluetooth is not enabled on this device");
        }
    }

    private class ConnectionListener implements BluetoothListener.Listener {
        private ProgressDialog progress;
        private boolean cancelled;

        public ConnectionListener() {
            this.progress = new ProgressDialog(PushActivity.this);
            this.progress.setTitle(R.string.dialog_title_pushing);
            this.progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.progress.setIndeterminate(true);
            this.progress.setMessage(getString(R.string.dialog_transferring));
            this.progress.setCancelable(true);
            this.progress.setCanceledOnTouchOutside(false);
            this.progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    cancelled = true;
                }
            });
        }

        @Override
        public void onConnect(int clients) {
            if (this.cancelled)
                return;

            if (clients > 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.show();
                    }
                });
            }
        }

        @Override
        public void onDisconnect(int clients) {
            if (this.cancelled)
                return;

            if (clients <= 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                    }
                });
            }
        }
    }
}
