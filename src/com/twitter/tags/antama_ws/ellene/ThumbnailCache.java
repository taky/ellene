package com.twitter.tags.antama_ws.ellene;

import java.io.*;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

public class ThumbnailCache {
    private Context c;
    private Album album;

    public ThumbnailCache(Context c, Album album) {
        this.c = c;
        this.album = album;
    }

    public BitmapDrawable get(File file) throws IOException {
        if (!this.album.thumbnailExists(file))
            this.update(file);

        return (BitmapDrawable)new BitmapFileReader(this.c, this.album.openThumbnail(file)).getRawDrawable();
    }

    public void update(File file) throws IOException {
        BitmapDrawable drawable = null;
        FileOutputStream os = null;

        try {
            drawable = (BitmapDrawable)new BitmapFileReader(this.c, file).getDrawable();

            Log.d("TC", String.format("generating thumbnail for %s", file.getName()));
            os = new FileOutputStream(this.album.openThumbnail(file));
            drawable.getBitmap().compress(Bitmap.CompressFormat.JPEG, 95, os);
            os.flush();
        }
        finally {
            if (drawable != null
                && drawable.getBitmap() != null)
                drawable.getBitmap().recycle();
            if (os != null)
                os.close();
        }
    }
}
