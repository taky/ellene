package com.twitter.tags.antama_ws.ellene;

import java.io.*;

import com.googlecode.jbencode.*;
import com.googlecode.jbencode.primitive.*;
import com.googlecode.jbencode.composite.*;

public class LiteralStringType extends StringType implements Comparable<LiteralStringType> {
    private final String value;

    public LiteralStringType(String value) {
        this.value = value;
    }

    @Override
        protected long getLength() {
        return value.length();
    }

    @Override
        protected void writeValue(OutputStream os) throws IOException {
        os.write(value.getBytes("US-ASCII"));
    }

    public int compareTo(LiteralStringType o) {
        return o.value.compareTo(value);
    }
}
