package com.twitter.tags.antama_ws.ellene;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AlbumReselector {
    private Context c;
    private Album album;

    public AlbumReselector(Context c, Album album) {
        this.c = c;
        this.album = album;
    }

    public void reselect() {
        SharedPreferences.Editor sp = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        sp.putString(this.c.getString(R.string.config_key_viewing_album), this.album.getRealName());
        sp.commit();
    }
}