package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;

import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRSender {
    private Activity activity;

    public QRSender(Activity activity, Album album) {
        this.activity = activity;
    }

    public BitmapDrawable send(String address) throws com.google.zxing.WriterException {
        if (address == null)
            return null;

        com.google.zxing.Writer writer = new QRCodeWriter();
        BitMatrix m = writer.encode(PullServer.getAdvertisementAsURL(address), com.google.zxing.BarcodeFormat.QR_CODE, 256, 256);

        Bitmap bitmap = Bitmap.createBitmap(m.getWidth(), m.getHeight(), Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0xffffffff);
        for (int w = 0; w < m.getWidth(); ++w) {
            for (int h = 0; h < m.getHeight(); ++h) {
                if (m.get(w, h))
                    bitmap.setPixel(w, h, 0xff000000);
            }
        }
        return new BitmapDrawable(this.activity.getResources(), bitmap);
    }

}
