package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.AsyncTask;
import android.widget.Toast;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.Gallery;
import android.widget.Toast;
import android.widget.EditText;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.io.*;
import java.util.*;

public class MainActivity extends Activity {

    private Album album;
    private ImageView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Intent intent = getIntent();
        this.album = new Album(this, null);

        if (!this.album.isAvailable()) {
            new StorageAlert(this).show();
            return;
        }

        if (Intent.ACTION_SEND.equals(intent.getAction())
            || Intent.ACTION_SEND_MULTIPLE.equals(intent.getAction())) {
            new AlbumCreator(this, new AlbumOperations()).startCreate();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.reload();
    }

    private void reload() {
        this.album = new AlbumLoader(this).load();

        View emptyIndicator = (View) findViewById(R.id.empty_indicator);
        Gallery gallery = (Gallery) findViewById(R.id.gallery);

        if (this.album.empty()) {
            gallery.setVisibility(View.GONE);
            emptyIndicator.setVisibility(View.VISIBLE);
        }
        else {
            emptyIndicator.setVisibility(View.GONE);
            gallery.setVisibility(View.VISIBLE);

            gallery.setAdapter(new AlbumImageAdapter(this, this.album));
            Toast.makeText(this, getString(R.string.toast_viewing_album, this.album.getName()), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_preferences:
            startActivity(new Intent(this, ConfigActivity.class));
            return true;
        case R.id.menu_rename:
            new AlbumRenamer(this, new AlbumOperations()).startRename();
            return true;
        case R.id.menu_delete:
            new AlbumDeleter(this, new AlbumOperations()).startDelete();
            return true;
        case R.id.menu_help:
            startActivity(new Intent(this, HelpActivity.class));
            return true;
        case R.id.menu_share:
            startActivity(new Intent(this, PushActivity.class));
            return true;
        }
        return true;
    }

    private class AlbumOperations implements AlbumOperator {
        private MainActivity activity = MainActivity.this;
        private Intent intent;

        public AlbumOperations() {
            this.intent = getIntent();
        }

        @Override
        public void cancel() {

        }

        @Override
        public void replace(Album newAlbum) {
            activity.album = newAlbum;
        }

        @Override
        public Album get() {
            return activity.album;
        }

        @Override
        public void rewrite() {
            new RewriteTask(activity.album).execute();
        }

        @Override
        public void delete() {
            activity.album.delete();

            try {
                List<Album> albums = new Album(activity, null).albums();
                new AlbumReselector(activity, albums.get(0)).reselect();
            }
            catch (IndexOutOfBoundsException e) {
                new AlbumReselector(activity, new Album(activity, null)).reselect();
            }

            activity.reload();
        }

        @Override
        public void rename(String name) {
            new Album(activity, name).delete();

            activity.album.rename(name);
            new AlbumReselector(activity, activity.album).reselect();

            activity.reload();
        }

        private class RewriteTask extends AsyncTask<Void, Integer, Void> {
            private Album album;
            private ProgressDialog dialog;

            public RewriteTask(Album album) {
                this.album = album;
            }

            @Override
            protected void onPreExecute() {
                if (this.dialog != null) {
                    this.dialog.dismiss();
                    this.dialog = null;
                }

                this.dialog = new ProgressDialog(activity);
                this.dialog.setTitle(R.string.dialog_title_loading);
                this.dialog.setMessage(getString(R.string.dialog_please_wait_1));
                this.dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                this.dialog.setIndeterminate(true);
                this.dialog.setCancelable(true);
                this.dialog.setCanceledOnTouchOutside(false);
                this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        RewriteTask.this.dialog = null;
                        RewriteTask.this.cancel(false);
                    }
                });
                this.dialog.show();
            }

            @Override
            protected Void doInBackground(Void... args) {
                this.rewrite();
                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
                int phase = progress[0];
                int sofar = progress[1];
                int total = progress[2];

                switch (phase) {
                case 0:
                    this.dialog.setIndeterminate(false);
                    this.dialog.setMessage(getString(R.string.dialog_please_wait_fetching));
                    this.dialog.setProgress(sofar);
                    this.dialog.setMax(total);
                    break;
                case 1:
                    this.dialog.setIndeterminate(false);
                    this.dialog.setMessage(getString(R.string.dialog_please_wait_caching));
                    this.dialog.setProgress(sofar);
                    this.dialog.setMax(total);
                    break;
                }
            }

            @Override
            protected void onPostExecute(Void arg) {
                this.dialog.dismiss();
                this.dialog = null;

                new AlbumReselector(activity, activity.album).reselect();

                activity.reload();
            }

            private void rewrite() {
                List<Parcelable> list = new LinkedList<Parcelable>();
                String action = intent.getAction();
                final Bundle extras = intent.getExtras();

                if (Intent.ACTION_SEND.equals(action))
                {
                    if (extras.containsKey(Intent.EXTRA_STREAM))
                        list.add(extras.getParcelable(Intent.EXTRA_STREAM));
                }
                if (Intent.ACTION_SEND_MULTIPLE.equals(action))
                {
                    if (extras.containsKey(Intent.EXTRA_STREAM)) {
                        for (Parcelable p : extras.getParcelableArrayList(Intent.EXTRA_STREAM))
                            list.add(p);
                    }
                }

                if (list.size() > 0) {
                    long total = 0;
                    long thumbnailed = 0;

                    try {
                        this.album.clear();
                        total = this.readAsAlbum(list);
                        publishProgress(1, 0, (int)total);
                        for (File file : this.album.list()) {
                            if (this.isCancelled())
                                throw new CancelledException();

                            try {
                                new ThumbnailCache(activity, this.album).update(file);
                            }
                            catch (IOException e) {
                            }
                            thumbnailed += file.length();
                            publishProgress(1, (int)thumbnailed, (int)total);
                        }
                    }
                    catch (CancelledException e) {
                        this.album.delete();
                    }
                }
            }

            private long readAsAlbum(List<Parcelable> list) throws CancelledException {
                int id = 0;
                long read = 0;
                long total = list.size();
                long totalSizeRead = 0;
                for (Parcelable p : list) {
                    if (this.isCancelled())
                        throw new CancelledException();

                    final String name = String.format("picture-%s", id);
                    try {
                        this.album.put(name, getContentResolver().openInputStream((Uri)p));
                        totalSizeRead += this.album.open(name).length();
                        ++id;
                    }
                    catch (FileNotFoundException e) {
                    }
                    catch (IOException e) {
                        final int currentId = id;
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.this, getString(R.string.toast_fetch_trouble, currentId), Toast.LENGTH_LONG).show();
                            }
                        });
                        this.album.open(name).delete();
                    }
                    ++read;
                    publishProgress(0, (int)read, (int)total);
                }
                return totalSizeRead;
            }

            private class CancelledException extends RuntimeException {
            }
        }
    }
}
