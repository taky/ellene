package com.twitter.tags.antama_ws.ellene;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.ListPreference;
import android.content.SharedPreferences;
import android.widget.Toast;
import android.app.Dialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.*;
import java.io.*;
import java.util.*;

public class ConfigActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    private ListPreference albums;
    private boolean isAlbumAvailable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.config);

        List<CharSequence> entries = new LinkedList<CharSequence>();
        List<CharSequence> entryValues = new LinkedList<CharSequence>();

        this.isAlbumAvailable = false;

        for (Album f : new Album(this, null).albums()) {
            String name = f.getName();
            entries.add(name);
            entryValues.add(name);
            this.isAlbumAvailable = true;
        }

        this.albums = (ListPreference)findPreference(getString(R.string.config_key_viewing_album));
        if (!this.isAlbumAvailable) {
            this.albums.setEnabled(false);
            entries.add(getString(R.string.default_album_name));
            entryValues.add(Album.PHANTOM);
        }

        this.albums.setEntries(entries.toArray(new CharSequence[0]));
        this.albums.setEntryValues(entryValues.toArray(new CharSequence[0]));

        Preference pref = findPreference(getString(R.string.config_key_app_version));
        pref.setOnPreferenceClickListener(new AboutListener());
    }

    @Override
    protected void onResume() {
        super.onResume();

		SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
		this.updateSummary(sharedPreferences, getString(R.string.config_key_viewing_album));

        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		this.updateSummary(sharedPreferences, key);
    }

	private void updateSummary(SharedPreferences sharedPreferences, String key)
	{
        if (key.equals(getString(R.string.config_key_viewing_album))) {
            String value = this.albums.getValue();
            if (value == null)
                this.albums.setValue(Album.PHANTOM);
			this.albums.setSummary(this.albums.getEntry());
        }
	}

    private class AboutListener implements Preference.OnPreferenceClickListener {
        @Override
        public boolean onPreferenceClick(Preference pref) {
            LayoutInflater inflater = ConfigActivity.this.getLayoutInflater();
            View layout = inflater.inflate(
                R.layout.about,
                (ViewGroup) ConfigActivity.this.findViewById(R.id.layout_root));

            AlertDialog.Builder builder = new AlertDialog.Builder(ConfigActivity.this);
            builder
                .setView(layout)
                .setTitle(R.string.app_name)
                .setPositiveButton(android.R.string.ok, new NullClickListener());
            builder.create().show();
            return true;
        }

        private class NullClickListener implements DialogInterface.OnClickListener {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }
    }
}
