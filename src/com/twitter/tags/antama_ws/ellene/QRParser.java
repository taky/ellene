package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import android.net.Uri;
import java.util.List;

public class QRParser {
    private List<String> params;

    public QRParser(Intent intent) {
        this.parse(intent);
    }

    private void parse(Intent intent) {
        if (intent == null)
            return;

        Uri uri = intent.getData();
        this.params = uri.getPathSegments();
    }

    public String getProtocolType() {
        try {
            return this.params.get(0);
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public String getBTPullPeerAddress() {
        try {
            String protocol = this.getProtocolType();
            if (protocol != null) {
                if (protocol.equals("ellene_pull_bt"))
                    return this.params.get(1).replace("-", ":");
            }
            return null;
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
