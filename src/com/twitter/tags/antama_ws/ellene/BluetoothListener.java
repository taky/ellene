package com.twitter.tags.antama_ws.ellene;

import java.io.*;
import java.net.*;
import java.util.UUID;
import android.bluetooth.*;
import android.util.Log;

public class BluetoothListener {
    public static interface Listener {
        public void onConnect(int clients);
        public void onDisconnect(int clients);
    }

    public static final String SERVICE_NAME = "ellene_bt_pull";
    public static final UUID SERVICE_ID = new UUID(0x9e9bc592a5232092L, 0x2fb9f7f2177d34bL);

    private Album album;
    private Thread thread = null;
    private BluetoothServerSocket socket = null;
    private Listener listener;
    private int clients = 0;

    public void start(Album album, Listener listener) throws IOException, BluetoothManipulator.NotSupportedException, BluetoothManipulator.NotEnabledException {
        if (this.thread != null)
            this.stop();

        BluetoothAdapter adapter = new BluetoothManipulator().setup();
        this.socket = adapter.listenUsingInsecureRfcommWithServiceRecord(SERVICE_NAME, SERVICE_ID);
        this.album = album;
        this.listener = listener;
        this.thread = new Thread(new Accepter());
        this.thread.start();
    }

    public void stop() {
        try {
            try {
                if (this.socket != null)
                    this.socket.close();
            }
            catch (IOException e) {
            }
            try {
                if (this.thread != null) {
                    this.thread.interrupt();
                    this.thread.join();
                }
            }
            catch (InterruptedException e) {
            }
        }
        finally {
            this.socket = null;
            this.thread = null;
        }
    }

    private class Accepter implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                Log.d("SL.SA.run", String.format("accepting connection"));
                try {
                    BluetoothSocket client = socket.accept();
                    if (client != null)
                        new Thread(new Server(client)).start();
                }
                catch (IOException e) {
                    Log.d("SL.L.run", String.format("cannot accept connection: %s", e.toString()));
                }
            }
        }
    }

    private class Server implements Runnable
    {
        private BluetoothSocket socket;

        public Server(BluetoothSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            Log.d("SL.SS.run", String.format("accepted connection"));
            try {
                synchronized (BluetoothListener.this) {
                    BluetoothListener.this.listener.onConnect(++BluetoothListener.this.clients);
                };
                try {
                    InputStream is = this.socket.getInputStream();
                    OutputStream os = this.socket.getOutputStream();
                    try {
                        PullServer server = new PullServer(album, is, os);
                        while (true) {
                            server.process();
                        }
                    }
                    catch (RuntimeException e) {
                        Log.d("SL.SS.run", String.format("cannot handle connection: protocol error: %s", StackTrace.get(e)));
                    }
                }
                catch (IOException e) {
                    Log.d("SL.SS.run", String.format("connection broken: %s", StackTrace.get(e)));
                }
            }
            finally {
                try {
                    this.socket.close();
                }
                catch (IOException e) {
                    Log.d("SL.SS.run", String.format("cannot close connection: %s", e.toString()));
                }
                synchronized (BluetoothListener.this) {
                    BluetoothListener.this.listener.onDisconnect(--BluetoothListener.this.clients);
                };
            }
        }
    }
}
