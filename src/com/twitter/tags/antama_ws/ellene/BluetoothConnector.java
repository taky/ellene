package com.twitter.tags.antama_ws.ellene;

import java.io.*;
import java.net.*;
import java.util.UUID;
import android.bluetooth.*;
import android.util.Log;

public class BluetoothConnector {
    private Album album;
    private Thread thread = null;
    private BluetoothSocket socket = null;
    private PullClient.Callback callback = null;

    private BluetoothDevice device;

    public void start(Album album, String address, PullClient.Callback callback) throws IOException, BluetoothManipulator.NotSupportedException, BluetoothManipulator.NotEnabledException {
        if (this.thread != null)
            this.stop();

        BluetoothAdapter adapter = new BluetoothManipulator().setup();
        this.device = adapter.getRemoteDevice(address);
        this.renewSocket();

        this.album = album;
        this.callback = callback;
        this.thread = new Thread(new Client());
        this.thread.start();
    }

    private void renewSocket() throws IOException {
        this.socket = this.device.createInsecureRfcommSocketToServiceRecord(BluetoothListener.SERVICE_ID);
    }

    public void stop() {
        try {
            try {
                if (this.socket != null)
                    this.socket.close();
            }
            catch (IOException e) {
            }
            try {
                if (this.thread != null) {
                    this.thread.interrupt();
                    this.thread.join();
                }
            }
            catch (InterruptedException e) {
            }
        }
        finally {
            this.socket = null;
            this.thread = null;
        }
    }

    private class Client implements Runnable {
        @Override
        public void run() {
            Log.d("BC.C.run", String.format("accepting connection"));
            try {
                try {
                    for (int i=0; ; ++i) {
                        try {
                            socket.connect();
                            break;
                        }
                        catch (IOException e) {
                            if (e.getMessage().contains("refused")) {
                                if (i < 3) {
                                    Log.d("BC.C.run", String.format("Uh oh, retrying (%d)...", i+1));
                                    socket.close();
                                    renewSocket();
                                    continue;
                                }
                                else {
                                    Log.d("BC.C.run", "Still facing connection refusal, given up!");
                                }
                            }
                            throw e;
                        }
                    }
                    new PullClient(album, socket.getInputStream(), socket.getOutputStream(), callback).pull();
                }
                catch (IOException e) {
                    Log.d("BC.C.run", String.format("connection broken: %s", StackTrace.get(e)));
                    if (callback != null)
                        callback.onFailure(e.getMessage());
                }
                catch (RuntimeException e) {
                    Log.d("BC.C.run", String.format("cannot pull: protocol error\n%s", StackTrace.get(e)));
                    if (callback != null)
                        callback.onFailure(e.getMessage());
                }
            }
            finally {
                try {
                    socket.close();
                }
                catch (IOException e) {
                    Log.d("BC.C.run", String.format("cannot close connection: %s", e.getMessage()));
                }
            }
        }
    }
}
