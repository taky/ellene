package com.twitter.tags.antama_ws.ellene;

import java.io.*;
import java.util.*;
import android.util.Log;
import com.googlecode.jbencode.*;
import com.googlecode.jbencode.primitive.*;
import com.googlecode.jbencode.composite.*;

public class PullServer {
    private InputStream is;
    private OutputStream os;
    private Album album;
    private Parser parser = new Parser();

    public PullServer(Album album, InputStream is, OutputStream os) {
        this.album = album;
        this.is = new BufferedInputStream(is);
        this.os = os;
    }

    public static String getAdvertisement(final String address) {
        Type root = new ListType() {
            @Override
            protected void populate(ListTypeStream list) throws IOException {
                list.add(new LiteralStringType("ellene_pull_bt"));
                list.add(new LiteralStringType(address));
            };
        };

        return new String(fixiate(root));
    }

    public static String getAdvertisementAsURL(final String address) {
        return String.format("content://com.twitter.tags.antama-ws.ellene/ellene_pull_bt/%s", address.replace(":", "-"));
    }

    private static byte[] fixiate(Type root) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            root.write(os);
            os.flush();
            return os.toByteArray();
        }
        catch (IOException e) {
            return "".getBytes();
        }
    }

    private void write(Type root) throws IOException {
        byte[] buffer = fixiate(root);
        for (int sofar = 0; sofar < buffer.length; ) {
            int length = Math.min(buffer.length - sofar, 16384);
            this.os.write(buffer, sofar, length);
            sofar += length;
        }
        this.os.flush();
    }

    public void process() throws IOException {
        try {
            String content = new String((byte[])this.parser.parse(this.is).resolve());
            Log.d("PS.p", String.format("resolved resource: %s", content));
            if (content.equals("/"))
                this.index();
            else
                this.serve(content);
        }
        catch (ClassCastException e) {
            Log.d("PS.p", "cannot resolve requested resource as string");
        }
    }

    public void index() throws IOException {
        final List<File> files = this.album.list();
        DictionaryType root = new DictionaryType() {

            @Override
            protected void populate(SortedSet<EntryType<?>> entries) {
                entries.add(
                    new EntryType<LiteralStringType>(
                        new LiteralStringType("_total"),
                        new IntegerType(files.size())
                    )
                );
                int totalSize = 0;
                for (File f : files)
                    totalSize += f.length();
                entries.add(
                    new EntryType<LiteralStringType>(
                        new LiteralStringType("_total_size"),
                        new IntegerType(totalSize)
                    )
                );
                entries.add(
                    new EntryType<LiteralStringType>(
                        new LiteralStringType("_order"),
                        new ListType() {
                            @Override
                            protected void populate(ListTypeStream list) throws IOException {
                                for (File f : files)
                                    list.add(new LiteralStringType(f.getName()));
                            }
                        }
                    )
                );
                for (File f : files) {
                    entries.add(
                        new EntryType<LiteralStringType>(
                            new LiteralStringType(f.getName()),
                            new IntegerType(f.length())
                        )
                    );
                }
            };
        };

        this.write(root);
    }

    public void serve(String content) throws IOException {
        Type root;
        try {
            File f = this.album.open(content);
            FileInputStream is = new FileInputStream(f);
            int length = (int)f.length();
            byte[] buffer = new byte[length];
            is.read(buffer);
            root = new ByteStringType(buffer, length);
        }
        catch (FileNotFoundException e) {
            root = new DictionaryType() {
                @Override
                protected void populate(SortedSet<EntryType<?>> entries) {
                    entries.add(
                        new EntryType<LiteralStringType>(
                            new LiteralStringType("error"),
                            new LiteralStringType("not-found")
                        )
                    );
                };
            };
        }
        catch (IOException e)   {
            root = new DictionaryType() {
                @Override
                protected void populate(SortedSet<EntryType<?>> entries) {
                    entries.add(
                        new EntryType<LiteralStringType>(
                            new LiteralStringType("error"),
                            new LiteralStringType("unexpected-io-error")
                        )
                    );
                };
            };
        }

        this.write(root);
    }
}
