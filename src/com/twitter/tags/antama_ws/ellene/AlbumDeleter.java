package com.twitter.tags.antama_ws.ellene;

import java.util.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import android.view.*;

public class AlbumDeleter {
    private Activity activity;
    private AlbumOperator operator;

    public AlbumDeleter(Activity activity, AlbumOperator operator) {
        this.activity = activity;
        this.operator = operator;
    }

    public void startDelete() {
        Album album = this.operator.get();

        if (album.isPhantom()) {
            Toast.makeText(this.activity, R.string.toast_no_album, Toast.LENGTH_LONG).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder
            .setMessage(R.string.dialog_title_delete_album)
            .setOnCancelListener(new CancelAction())
            .setNegativeButton(android.R.string.cancel, new CancelAction())
            .setPositiveButton(android.R.string.ok, new ConfirmAction());
        builder.create().show();
    }

    private class CancelAction implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
        @Override
        public void onCancel(DialogInterface dialog) {

        }
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    }

    private class ConfirmAction implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            operator.delete();
        }
    }
}
