package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.nfc.*;
import android.util.*;
import java.util.*;
import java.io.*;
import com.googlecode.jbencode.*;
import com.googlecode.jbencode.primitive.*;
import com.googlecode.jbencode.composite.*;

public class NFCParser {
    private List<String> params = new LinkedList<String>();

    public NFCParser(Intent intent) {
        this.parse(intent);
    }

    private void parse(Intent intent) {
        if (intent == null)
            return;

        try {
            Parcelable[] rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMessages == null)
                return;

            List<NdefMessage> messages = new LinkedList<NdefMessage>();
            for (Parcelable p : rawMessages)
                messages.add((NdefMessage)p);
            List<NdefRecord> records = Arrays.asList(messages.get(0).getRecords());
            NdefRecord record = records.get(0);
            Parser parser = new Parser();
            InputStream is = new ByteArrayInputStream(record.getPayload());
            try {
                ListValue contents = (ListValue)parser.parse(is);
                for (Value<?> entry : contents) {
                    String value = new String(((StringValue)entry).resolve());
                    Log.d("NFCP.p", String.format("parsed: %s", value));
                    this.params.add(value);
                }
            }
            catch (ClassCastException e) {
                return;
            }
        }
        catch (IndexOutOfBoundsException e) {
            return;
        }
        catch (RuntimeException e) {
            return;
        }
        catch (IOException e) {
            return;
        }
    }

    public String getProtocolType() {
        try {
            return this.params.get(0);
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public String getBTPullPeerAddress() {
        try {
            String protocol = this.getProtocolType();
            if (protocol != null) {
                if (protocol.equals("ellene_pull_bt"))
                    return this.params.get(1);
            }
            return null;
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
