package com.twitter.tags.antama_ws.ellene;

import android.content.Context;
import android.view.Display;
import android.view.WindowManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class BitmapFileReader {
    private File file;
    private Context context;

    private int maxWidth;
    private int maxHeight;

    public BitmapFileReader(Context context, File file) {
        this.context = context;
        this.file = file;
        this.init();
    }

    private void init() {
        WindowManager wm = (WindowManager)this.context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        this.maxWidth = display.getWidth();
        this.maxHeight = display.getHeight();
    }

    public Drawable getDrawable() throws IOException {
        Bitmap bitmap = null;
        Bitmap scaled = null;
        BitmapFactory.Options bfo = new BitmapFactory.Options();
        bfo.inSampleSize = this.findSubsampleFactor();
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(this.file), null, bfo);
            scaled = this.scale(bitmap);
            return new BitmapDrawable(this.context.getResources(), scaled);
        }
        finally {
            if (bitmap != null && bitmap != scaled)
                bitmap.recycle();
        }
    }

    public Drawable getRawDrawable() throws IOException {
        Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(this.file));
        return new BitmapDrawable(this.context.getResources(), bitmap);
    }

    private Bitmap scale(Bitmap src)
    {
        int width = src.getWidth();
        int height = src.getHeight();
        if (this.maxWidth < 0 || this.maxHeight < 0)
            return src;
        if (width < this.maxWidth && height < this.maxHeight)
            return src;

        if (width > height)
        {
            height = (int)(height * (this.maxWidth / (float)width));
            width = this.maxWidth;
        }
        if (width < height)
        {
            width = (int)(width * (this.maxHeight / (float)height));
            height = this.maxHeight;
        }
        Log.d("BFR", String.format("scaling: (%d, %d) -> (%d, %d)", src.getWidth(), src.getHeight(), width, height));
        return Bitmap.createScaledBitmap(src, width, height, true);
    }

    private int findSubsampleFactor() throws IOException {
        BitmapFactory.Options bfo = new BitmapFactory.Options();
        bfo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(new FileInputStream(this.file), null, bfo);
        return (int)Math.floor(bfo.outWidth / (float)maxWidth);
    }
}
