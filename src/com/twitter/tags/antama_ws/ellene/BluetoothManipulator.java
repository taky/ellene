package com.twitter.tags.antama_ws.ellene;

import android.bluetooth.*;

public class BluetoothManipulator {
    public BluetoothAdapter setup() throws NotSupportedException, NotEnabledException {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null)
            throw new NotSupportedException();

        if (!adapter.isEnabled())
            throw new NotEnabledException();
        return adapter;
    }

    public static class NotSupportedException extends Exception {
    }

    public static class NotEnabledException extends Exception {
    }
}
