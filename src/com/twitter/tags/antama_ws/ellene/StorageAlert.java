package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.view.*;

public class StorageAlert {
    private Activity activity;

    public StorageAlert(Activity activity) {
        this.activity = activity;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder
            .setTitle(R.string.app_name)
            .setMessage(R.string.dialog_no_external_storage)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    activity.finish();
                }
            });
        builder.create().show();
    }
}