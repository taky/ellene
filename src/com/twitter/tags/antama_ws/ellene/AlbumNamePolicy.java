package com.twitter.tags.antama_ws.ellene;

import java.util.Date;
import java.text.SimpleDateFormat;

public class AlbumNamePolicy {
    private Album album;

    public AlbumNamePolicy(Album currentAlbum) {
        this.album = currentAlbum;
    }

    public String getDefault() {
        return new SimpleDateFormat("yyyy.MM.dd HH.mm.ss").format(new Date());
    }

    public String format(String pattern) throws InvalidException {
        String stage = pattern;

        if (pattern.equals(Album.PHANTOM))
            throw new InvalidException();

        if (stage.contains("$date"))
            stage = stage.replace("$date", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        if (stage.contains("#"))
            stage = stage.replace("#", String.format("%d", this.album.albums().size() + 1));
        return stage;
    }

    public static class InvalidException extends Exception {
    }
}
