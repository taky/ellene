package com.twitter.tags.antama_ws.ellene;

import java.io.*;

public final class ProgressReportingInputStream extends FilterInputStream
{
	public interface ProgressListener
	{
		void onAdvance(long at, long length);
	}

	private int marked = 0;
	private long position = 0;
	private ProgressListener listener;

	public ProgressReportingInputStream(final InputStream in, final ProgressListener listener)
	{
		super(in);
		this.listener = listener;
	}

	@Override
	public int read() throws IOException
	{
		final int ch = super.read();
		this.position += 1;
		this.report();
		return ch;
	}

	@Override
	public int read(byte[] buffer, int offset, int count) throws IOException
	{
		final int advanced = super.read(buffer, offset, count);
		this.position += advanced;
		this.report();
		return advanced;
	}

	@Override
	public synchronized void reset() throws IOException
	{
		super.reset();
		this.position = this.marked;
	}

	@Override
	public synchronized void mark(int readlimit)
	{
		super.mark(readlimit);
		this.marked = readlimit;
	}

	@Override
	public long skip(long byteCount) throws IOException
	{
		long advanced = super.skip(byteCount);
		this.position += advanced;
		this.report();
		return advanced;
	}

	private void report()
	{
		if (this.listener == null)
			return;

		try
		{
			this.listener.onAdvance(this.position, this.position + this.in.available());
		}
		catch (IOException e)
		{
			this.listener.onAdvance(this.position, 0);
		}
	}
}
