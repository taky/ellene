package com.twitter.tags.antama_ws.ellene;

import java.util.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.view.*;

public class AlbumCreator {
    private Activity activity;
    private AlbumOperator operator;

    public AlbumCreator(Activity activity, AlbumOperator operator) {
        this.activity = activity;
        this.operator = operator;
    }

    public void startCreate() {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(
            R.layout.new_album,
            (ViewGroup) this.activity.findViewById(R.id.layout_root));

        Album album = operator.get();
        EditText field = (EditText)layout.findViewById(R.id.name);
        field.setText(new AlbumNamePolicy(album).getDefault());

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder
            .setView(layout)
            .setTitle(R.string.dialog_title_new_album)
            .setOnCancelListener(new CancelAction())
            .setNegativeButton(android.R.string.cancel, new CancelAction())
            .setPositiveButton(android.R.string.ok, new CreateAsNewAction(field));
        builder.create().show();
    }

    private class CancelAction implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            operator.cancel();
        }
        @Override
        public void onCancel(DialogInterface dialog) {
            dialog.dismiss();
            operator.cancel();
        }
    }


    private class CreateAsNewAction implements DialogInterface.OnClickListener {
        private EditText field;

        public CreateAsNewAction(EditText field) {
            this.field = field;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            String name = this.field.getText().toString();
            try {
                Album album = operator.get();

                name = new AlbumNamePolicy(album).format(name);
                dialog.dismiss();

                if (new Album(activity, name).exists()) {
                    this.showConfirmation(name);
                    return;
                }

                writeAs(name);
            }
            catch (AlbumNamePolicy.InvalidException e) {
                this.deny(name);
            }
        }

        private void writeAs(String name) {
            Album album = new Album(activity, name);
            album.open();
            operator.replace(album);
            operator.rewrite();
        }

        private void deny(String newAlbumName) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder
                .setMessage(activity.getString(R.string.dialog_invalid_name, newAlbumName))
                .setOnCancelListener(new RetryAction())
                .setPositiveButton(android.R.string.ok, new RetryAction());
            builder.create().show();
        }

        private void showConfirmation(String newAlbumName) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder
                .setMessage(R.string.dialog_title_confirm)
                .setOnCancelListener(new CancelAction())
                .setNegativeButton(android.R.string.no, new CancelAction())
                .setPositiveButton(android.R.string.yes, new WriteAction(newAlbumName));
            builder.create().show();
        }

        private class WriteAction implements DialogInterface.OnClickListener {
            private String name;

            public WriteAction(String name) {
                this.name = name;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                writeAs(this.name);
            }
        }

        private class RetryAction implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
            @Override
            public void onCancel(DialogInterface dialog) {
                this.retry(dialog);
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                this.retry(dialog);
            }

            private void retry(DialogInterface dialog) {
                dialog.dismiss();
                startCreate();
            }
        }
    }
}
