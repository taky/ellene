package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.Toast;
import android.os.Bundle;
import android.content.Intent;
import android.widget.Button;
import android.view.View;
import android.util.Log;

import android.content.Intent;
import android.bluetooth.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class PullActivity extends Activity {
    private Album album;
    private PullClient client;
    private BluetoothAdapter adapter;
    private BluetoothConnector connector = new BluetoothConnector();

    private static final int ENABLE_BT_REQUEST = 1;
    private String address;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        new WakeLock(this).hold();

        Intent intent = getIntent();

        if (intent.getAction().equals("android.nfc.action.NDEF_DISCOVERED")) {
            this.address = new NFCParser(getIntent()).getBTPullPeerAddress();
        }
        else {
            this.address = new QRParser(getIntent()).getBTPullPeerAddress();
        }

        if (this.address == null) {
            Log.d("PA.oC", "cannot pull: cannot determine peer address");
            finish();
            return;
        }

        this.album = new Album(this, null);

        if (!this.album.isAvailable()) {
            new StorageAlert(this).show();
            return;
        }

        new AlbumCreator(this, new AlbumOperations()).startCreate();
    }

    private class AlbumOperations implements AlbumOperator {
        @Override
        public void cancel() {
            Log.d("PA.oC", "pull cancelled");
            finish();
        }

        @Override
        public void rewrite() {
            startPull();
        }

        @Override
        public Album get() {
            return PullActivity.this.album;
        }

        @Override
        public void replace(Album newAlbum) {
            PullActivity.this.album = newAlbum;
        }

        @Override
        public void delete() {
        }

        @Override
        public void rename(String name) {
        }
    }

    private void startPull() {
        try {
            this.adapter = new BluetoothManipulator().setup();
            this.pull(this.address);
        }
        catch (BluetoothManipulator.NotSupportedException e) {
            Log.d("PA.sB", "bluetooth is not supported");
            finish();
        }
        catch (BluetoothManipulator.NotEnabledException e) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case ENABLE_BT_REQUEST:
            if (resultCode == RESULT_OK) {
                this.startPull();
            }
            else {
                Log.d("PA.oAR", "bluetooth cannot be enabled");
                finish();
            }
            break;
        default:
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void pull(String address) {
        try {
            ProgressUpdater progress = new ProgressUpdater();
            progress.init();
            this.connector.start(this.album, address, progress);
        }
        catch (IOException e) {
            Log.d("PA.pull", String.format("cannot pull: %s", e.toString()));
        }
        catch (BluetoothManipulator.NotSupportedException e) {
            Log.d("PA.pull", "bluetooth is not supported on this device");
        }
        catch (BluetoothManipulator.NotEnabledException e) {
            Log.d("PA.pull", "bluetooth is not enabled on this device");
        }
    }

    private class ProgressUpdater implements PullClient.Callback {
        private ProgressDialog dialog;
        private Map<String, ?> map;
        private int loaded = 0;
        private int loadedBytes = 0;
        private int max = 0;
        private int maxBytes = 0;
        private boolean cancelled = false;

        public void init() {
            this.dialog = new ProgressDialog(PullActivity.this);
            this.dialog.setTitle(R.string.dialog_title_retrieving);
            this.dialog.setMessage(getString(R.string.dialog_please_wait_1));
            this.dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.dialog.setIndeterminate(true);
            this.dialog.setCancelable(true);
            this.dialog.setCanceledOnTouchOutside(false);
            this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Log.d("PA.PU", "cancel requested, pull client confusion will follow");
                    cancelled = true;
                    PullActivity.this.connector.stop();
                    PullActivity.this.finish();
                }
            });
            this.dialog.show();
        }

        public void dismiss() {
            this.dialog.dismiss();
        }

        public void onIndexRetrieved(final Map<String, ?> index) {
            runOnUiThread(new Runnable() {
                public void run() {
                    map = index;
                    loaded = 0;
                    loadedBytes = 0;
                    max = ((Long)map.get("_total")).intValue();
                    maxBytes = ((Long)map.get("_total_size")).intValue();
                    dialog.setIndeterminate(false);
                    dialog.setMessage(getString(R.string.dialog_please_wait_2, loaded + 1, max));
                    dialog.setMax(maxBytes);
                    dialog.setProgress(0);
                }
            });
        }
        public void onFileLoading(final String key, final long sizeLoaded) {
            runOnUiThread(new Runnable() {
                public void run() {
                    dialog.setProgress(loadedBytes + (int)sizeLoaded);
                }
            });
        }
        public void onFileLoaded(final String key) {
            runOnUiThread(new Runnable() {
                public void run() {
                    loaded += 1;
                    loadedBytes += ((Long)map.get(key)).intValue();
                    dialog.setProgress(loadedBytes);
                    if (loaded < max)
                        dialog.setMessage(getString(R.string.dialog_please_wait_2, loaded + 1, max));
                }
            });
        }
        public void onSuccess() {
            runOnUiThread(new Runnable() {
                public void run() {
                    new AlbumReselector(PullActivity.this, PullActivity.this.album).reselect();
                    dismiss();
                    finish();

                    Intent intent = new Intent(PullActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        }
        public void onFailure(final String reason) {
            runOnUiThread(new Runnable() {
                public void run() {
                    PullActivity.this.album.delete();
                    dismiss();
                    if (!cancelled)
                        Toast.makeText(PullActivity.this, getString(R.string.toast_pull_trouble, reason), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }
}
