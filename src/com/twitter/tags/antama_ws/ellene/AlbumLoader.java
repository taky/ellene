package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AlbumLoader {
    private Activity activity;

    public AlbumLoader(Activity activity) {
        this.activity = activity;
    }

    public Album load() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.activity);
        String albumName = sp.getString(this.activity.getString(R.string.config_key_viewing_album), null);
        return new Album(this.activity, albumName);
    }
}

