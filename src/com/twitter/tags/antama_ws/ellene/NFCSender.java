package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.nfc.*;
import java.util.*;
import java.nio.charset.Charset;

public class NFCSender {
    private Activity activity;
    private boolean enabled;
    private NfcAdapter adapter;
    private NdefMessage message;

    public NFCSender(Activity activity, Album album) {
        this.activity = activity;
    }

    public void init() {
        this.adapter = NfcAdapter.getDefaultAdapter(this.activity);
    }

    public void send(String address) {
        if (address == null)
            return;

        this.message = new NdefMessage(
            new NdefRecord[] {
                this.createMimeRecord("application/com.twitter.tags.antama_ws.ellene", PullServer.getAdvertisement(address).getBytes())
            }
        );
        if (this.enabled)
            this.resume();
        this.enabled = true;
    }

    // From SDK example
    private NdefRecord createMimeRecord(String mimeType, byte[] payload) {
        byte[] mimeBytes = mimeType.getBytes(Charset.forName("US-ASCII"));
        NdefRecord mimeRecord = new NdefRecord(
            NdefRecord.TNF_MIME_MEDIA, mimeBytes, new byte[0], payload);
        return mimeRecord;
    }

    public void stop() {
        this.pause();
        this.enabled = false;
    }

    public void resume() {
        if (this.adapter != null) {
            if (this.enabled)
                this.adapter.enableForegroundNdefPush(this.activity, this.message);
        }
    }

    public void pause() {
        if (this.adapter != null) {
            if (this.enabled)
                this.adapter.disableForegroundNdefPush(this.activity);
        }
    }
}
