package com.twitter.tags.antama_ws.ellene;

import java.util.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import android.view.*;

public class AlbumRenamer {
    private Activity activity;
    private AlbumOperator operator;

    public AlbumRenamer(Activity activity, AlbumOperator operator) {
        this.activity = activity;
        this.operator = operator;
    }

    public void startRename() {
        Album album = this.operator.get();
        if (album.isPhantom()) {
            Toast.makeText(this.activity, R.string.toast_no_album, Toast.LENGTH_LONG).show();
            return;
        }

        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(
            R.layout.rename_album,
            (ViewGroup) this.activity.findViewById(R.id.layout_root));

        EditText field = (EditText)layout.findViewById(R.id.name);
        field.setText(album.getName());

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder
            .setView(layout)
            .setTitle(R.string.dialog_title_rename_album)
            .setOnCancelListener(new CancelAction())
            .setNegativeButton(android.R.string.cancel, new CancelAction())
            .setPositiveButton(android.R.string.ok, new ConfirmAction(field));
        builder.create().show();
    }

    private class CancelAction implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
        @Override
        public void onCancel(DialogInterface dialog) {

        }
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    }

    private class ConfirmAction implements DialogInterface.OnClickListener {
        private EditText field;

        public ConfirmAction(EditText field) {
            this.field = field;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            String name = this.field.getText().toString();

            try {
                Album album = operator.get();
                name = new AlbumNamePolicy(album).format(name);

                if (name.equals(album.getName()))
                    return;

                if (new Album(activity, name).exists()) {
                    this.showConfirmation(name);
                    return;
                }

                this.renameAs(name);
            }
            catch (AlbumNamePolicy.InvalidException e) {
                this.deny(name);
            }
        }

        private void renameAs(String name) {
            operator.rename(name);
        }

        private void deny(String newAlbumName) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder
                .setMessage(activity.getString(R.string.dialog_invalid_name, newAlbumName))
                .setOnCancelListener(new RetryAction())
                .setPositiveButton(android.R.string.ok, new RetryAction());
            builder.create().show();
        }

        private void showConfirmation(String newAlbumName) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder
                .setMessage(R.string.dialog_title_confirm)
                .setOnCancelListener(new CancelAction())
                .setNegativeButton(android.R.string.no, new CancelAction())
                .setPositiveButton(android.R.string.yes, new RenameAction(newAlbumName));
            builder.create().show();
        }

        private class RenameAction implements DialogInterface.OnClickListener {
            private String name;

            public RenameAction(String name) {
                this.name = name;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                renameAs(this.name);
            }
        }

        private class RetryAction implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
            @Override
            public void onCancel(DialogInterface dialog) {
                this.retry(dialog);
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                this.retry(dialog);
            }

            private void retry(DialogInterface dialog) {
                dialog.dismiss();
                startRename();
            }
        }
    }
}
