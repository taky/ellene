package com.twitter.tags.antama_ws.ellene;

import java.io.*;
import java.util.*;
import android.util.Log;
import com.googlecode.jbencode.*;
import com.googlecode.jbencode.primitive.*;
import com.googlecode.jbencode.composite.*;

public class PullClient {
    public interface Callback {
        public void onIndexRetrieved(final Map<String, ?> index);
        public void onFileLoading(final String key, long sizeLoaded);
        public void onFileLoaded(final String key);
        public void onSuccess();
        public void onFailure(final String reason);
    }

    private static final class NullCallback implements Callback {
        public void onIndexRetrieved(final Map<String, ?> index) {
        }
        public void onFileLoading(final String key, long sizeLoaded) {
        }
        public void onFileLoaded(final String key) {
        }
        public void onSuccess() {
        }
        public void onFailure(final String reason) {
        }
    }

    private InputStream ris;
    private InputStream is;
    private OutputStream os;
    private Album album;
    private Callback callback = new NullCallback();
    private Parser parser = new Parser();

    public PullClient(Album album, InputStream is, OutputStream os, Callback callback) {
        this.album = album;
        this.ris = is;
        this.is = new BufferedInputStream(is);
        this.os = new BufferedOutputStream(os);
        if (callback != null)
            this.callback = callback;
    }

    private static String writeAsString(Type root) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            write(root, os);
            return os.toString();
        }
        catch (IOException e) {
            return "";
        }
    }

    private static void write(Type root, OutputStream os) throws IOException {
        root.write(os);
        os.flush();
    }

    private void write(Type root) throws IOException {
        write(root, this.os);
    }

    public void pull() throws IOException {
        Map<String, ?> map = this.getIndex();
        this.callback.onIndexRetrieved(map);

        long totalCount = (Long)map.get("_total");
        long totalSize = (Long)map.get("_total_size");
        List<String> order = (List<String>)map.get("_order");

        this.album.clear();

        for (String key : order) {
            long length = (Long)map.get(key);

            Log.d("PC.p", String.format("resource: %s -> %d bytes", key, length));
            try {
                this.album.put(key, this.getBlob(key));
                this.callback.onFileLoaded(key);
                Log.d("PC.p", String.format("new: %s (%d bytes)", key, length));
            }
            catch (IOException e) {
                Log.d("PC.p", String.format("cannot put picture in album: %s", e.getMessage()));
                this.callback.onFailure(e.getMessage());
                return;
            }
        }

        this.callback.onSuccess();
    }

    private Map<String, ?> getIndex() throws IOException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            Log.d("PC.gI", "listing resources");

            Type root = new LiteralStringType("/");
            this.write(root);

            DictionaryValue contents = (DictionaryValue)this.parser.parse(this.is);
            for (EntryValue entry : contents) {
                String key = new String(entry.getKey().resolve());
                try {
                    long length = ((IntegerValue)entry.getValue()).resolve();
                    Log.d("PC.gI", String.format("resource found: %s (%d)", key, length));
                    map.put(key, new Long(length));
                }
                catch (ClassCastException e) {
                    List<String> list = new LinkedList<String>();
                    for (Value<?> v : (ListValue)entry.getValue())
                        list.add(new String((byte[])v.resolve(), "UTF-8"));
                    map.put(key, list);
                }
            }

            return map;
        }
        catch (ClassCastException e) {
            Log.d("PC.gI", "cannot resolve requested resource as string");
            return null;
        }
    }

    private byte[] getBlob(final String key) throws IOException {
        try {
            Log.d("PC.gB", String.format("requesting resource: %s", key));

            Type root = new LiteralStringType(key);
            this.write(root);

            this.callback.onFileLoading(key, 0);
            InputStream pris = new BufferedInputStream(
                new ProgressReportingInputStream(
                    this.ris, new ProgressReportingInputStream.ProgressListener() {
                        private long reported = 0;

                        @Override
                        public void onAdvance(long at, long length) {
                            if ((at - this.reported) > 16*1024) {
                                this.reported = at;
                                callback.onFileLoading(key, at);
                            }
                        };
                    }
                )
            );

            byte[] blob = ((StringValue)this.parser.parse(pris)).resolve();
            Log.d("PC.gB", String.format("got resource: %s", key));
            return blob;
        }
        catch (ClassCastException e)
        {
            Log.d("PC.gI", "cannot resolve requested resource as blob");
            return null;
        }
    }
}
