package com.twitter.tags.antama_ws.ellene;

import android.app.*;
import java.io.*;
import java.util.*;
import android.net.Uri;
import android.util.Log;

public class Album {
    public static final String PHANTOM = "____default____";

    private Activity activity;
    private String name;

    public Album(Activity activity, String name) {
        this.activity = activity;
        this.name = name;
        if (this.name == null)
            this.name = Album.PHANTOM;
    }

    public boolean isAvailable() {
        return this.root() != null;
    }

    public boolean isPhantom() {
        return !this.isAvailable() || (this.name.equals(Album.PHANTOM));
    }

    public String getName() {
        if (this.name != null)
            return this.name;
        else
            return this.activity.getString(R.string.default_album_name);
    }

    public String getRealName() {
        return this.name;
    }

    public List<Album> albums() {
        List<Album> out = new LinkedList<Album>();
        try {
            for (File f : this.root().listFiles(new SensibleFilter()))
                out.add(new Album(this.activity, toAlbumName(f.getName())));
            return out;
        }
        catch (NullPointerException e) {
            return out;
        }
    }

    public List<File> list() {
        try {
            return Arrays.asList(this.open().listFiles(new SensibleFilter()));
        }
        catch (NullPointerException e) {
            return new LinkedList<File>();
        }
    }

    private List<File> listAll() {
        try {
            return Arrays.asList(this.open().listFiles());
        }
        catch (NullPointerException e) {
            return new LinkedList<File>();
        }
    }

    public boolean exists() {
        return new File(this.root(), toPathName(this.name)).exists();
    }

    public boolean empty() {
        return this.list().size() == 0;
    }

    public File root() {
        return this.activity.getExternalFilesDir(null);
    }

    public File open() {
        File file = new File(this.root(), toPathName(this.name));
        file.mkdir();
        return file;
    }

    public File open(String name) {
        return new File(this.open(), name);
    }

    public void delete() {
        for (File f : this.listAll())
            f.delete();
        new File(this.root(), toPathName(this.name)).delete();
    }

    public void rename(String newName) {
        new File(this.root(), toPathName(this.name)).renameTo(new File(this.root(), toPathName(newName)));
        this.name = newName;
    }

    public void put(String name, InputStream is) throws IOException {
        long total = 0;
        final byte[] buffer = new byte[16384];
        FileOutputStream stream = new FileOutputStream(this.open(name));
        try {
            while (true) {
                int read = is.read(buffer, 0, 16384);
                if (read < 0)
                    break;
                stream.write(buffer, 0, read);
                total += read;
            }
            if (total == 0)
                throw new IOException("Empty stream detected; suspected Picasa sabotage");
        }
        finally {
            stream.flush();
        }
    }

    public void put(String name, byte[] blob) throws IOException {
        FileOutputStream stream = new FileOutputStream(this.open(name));
        stream.write(blob, 0, blob.length);
        stream.flush();
    }

    public boolean thumbnailExists(File file) throws IOException {
        return this.open(String.format("%s.cache", file.getName())).exists();
    }

    public File openThumbnail(File file) throws IOException {
        return this.open(String.format("%s.cache", file.getName()));
    }

    public void clear() {
        for (File f : this.listAll())
            f.delete();
        this.open();
    }

    private static String toPathName(String albumName) {
        if (!albumName.equals(Album.PHANTOM)) {
            return Uri.encode(albumName).replaceFirst("^\\.", "%2E");
        }
        else
            return albumName;
    }

    private static String toAlbumName(String pathName) {
        if (!pathName.equals(Album.PHANTOM))
            return Uri.decode(pathName);
        else
            return pathName;
    }

    private class SensibleFilter implements FileFilter {
        @Override
        public boolean accept(File file) {
            return !file.getName().endsWith(".cache") && !file.getName().equals(Album.PHANTOM);
        }
    }
}
