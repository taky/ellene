package com.twitter.tags.antama_ws.ellene;

import java.io.*;

import com.googlecode.jbencode.*;
import com.googlecode.jbencode.primitive.*;
import com.googlecode.jbencode.composite.*;

public class ByteStringType extends StringType {
    private final byte[] value;
    private long length;

    public ByteStringType(byte[] value, long length) {
        this.value = value;
        this.length = length;
    }

    @Override
    protected long getLength() {
        return this.length;
    }

    @Override
    protected void writeValue(OutputStream os) throws IOException {
        os.write(value);
    }
}
