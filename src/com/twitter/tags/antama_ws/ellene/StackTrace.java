package com.twitter.tags.antama_ws.ellene;

import java.io.*;

public class StackTrace {
    public static String get(final Exception e) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        return result.toString();
    }
}
